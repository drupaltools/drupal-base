# drupal-base

Drupal base for starting new projects.

INSTALL
---------

Download the project.
```composer create-project edwinknol/drupal-base:dev-master some-dir --no-interaction```

Install the project.
```vendor/bin/drush site-install --account-name=admin --account-pass=admin --account-mail=noreply@local.local --site-mail=admin@dev.drupal.test --existing-config -y -vvv```

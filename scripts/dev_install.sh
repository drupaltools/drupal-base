#!/usr/bin/env bash
set -e

if [ -e config/sync ]; then
    echo "Installing from existing config"
    drush si -y --existing-config
else
    echo "New installation"
	drush si -y ods;
fi

echo "Exporting configuration to ensure no configuration gets lost and/or mixed up"
drush cex -y;

#!/usr/bin/env bash
set -e

WEBSITE=retrogame-nl
BRANCH=$(git rev-parse --symbolic-full-name --abbrev-ref HEAD)

read -p "branch/tag/hash to build [${BRANCH}]: " REF
REF=${REF:-${BRANCH}}
BUILD_DIR=${PWD}/build
RELEASE_DIR=${BUILD_DIR}/release

mkdir -p "${BUILD_DIR}"
rm -Rf "${RELEASE_DIR}"
git clone --shared --local . "${RELEASE_DIR}"

echo Checking out "${REF}"
cd "${RELEASE_DIR}"
git fetch -q
git checkout -q "${REF}"

RELEASE=$(git describe --tags --always);
BUILD=${WEBSITE}-${RELEASE}.tar.gz;

composer install --no-dev --optimize-autoloader --prefer-dist

rm -Rf .git
rm config/*.local.php .gitignore composer.lock Makefile

echo Making build "${RELEASE}"
touch "${BUILD}"
tar --exclude="${BUILD}" -czf "${BUILD}" .
mv "${BUILD}" "${BUILD_DIR}"
rm -Rf "${RELEASE_DIR}"

echo
echo "🎉" ${WEBSITE} release "${RELEASE}" built
echo

#!/usr/bin/env bash
set -e

WEBSITE=retrogame.nl
ACCEPT_SERVER=46.226.110.50
PROD_SERVER=46.226.110.50
DATE=$(date '+%Y%m%d-%H%M')
DEPLOY_TARGET=/var/www/${WEBSITE}/public_html
DEPLOY_DIR=${DEPLOY_TARGET}/${DATE}
BACKUP_TARGET=/var/www/backups/${WEBSITE}
BACKUP_DIR=${BACKUP_TARGET}/${DATE}
DRUPAL_USER=root
HTTPD_GROUP=nginx
DRUSH=${DEPLOY_DIR}"/vendor/bin/drush --root=${DEPLOY_DIR}/public"
PHP_PATH="/opt/remi/php71/root/bin"

echo Select environment to deploy to:
select DESTINATION in Acceptance Production Cancel
do
  case ${DESTINATION} in
    Acceptance)
      SSH_NAME=${DRUPAL_USER}@${ACCEPT_SERVER}
      echo You have chosen to deploy to the Acceptance environment.
      break;;
    Production)
      SSH_NAME=${DRUPAL_USER}@${PROD_SERVER}
      echo You have chosen to deploy to the Production environment.
      break;;
    Cancel)
      exit;;
    *)
      echo Invalid option
      ;;
  esac
done

if [[ ! -d build/ ]]; then
  echo No local build directory found. Did you run \'make build\'?
  exit 1
fi

declare -a options
i=0
while IFS= read -r f; do
  options[i++]=${f}
done < <(cd build/ && find . -maxdepth 1 -type f -name "*.tar.gz" | xargs stat -f '%m%N' | sed "s/[0-9][0-9]*\.\///g")

echo
echo Select build package:
select BUILD_PACKAGE in ${options[@]} Cancel
do
  case ${BUILD_PACKAGE} in
    *.tar.gz)
      echo ${BUILD_PACKAGE} selected
      break;;
    Cancel)
      exit;;
    *)
      echo This is not a valid number
      ;;
  esac
done

echo
echo Check current remote config
RESULT=`ssh -T ${SSH_NAME} << EOF
if [[ ! -f ${DEPLOY_TARGET}/current/config/settings.local.php ]]; then
  echo 1
fi
EOF`

if [[ ${RESULT} -gt 0 ]]; then
  echo Remote environment is not configured correctly. /config/settings.local.php is missing. Exiting.
  exit 1;
fi

echo Upload build package
scp build/${BUILD_PACKAGE} ${SSH_NAME}:/tmp

ssh -T ${SSH_NAME} << EOF
  export PATH=${PHP_PATH}:$PATH

  if [[ ! -f /tmp/${BUILD_PACKAGE} ]]; then
    echo No remote build package found. Exiting.
    exit 1
  fi

  echo Create deploy directory
  mkdir -p ${DEPLOY_DIR}
  cd ${DEPLOY_DIR}
  touch ${DEPLOY_DIR}/.DEPLOY

  echo Extract build package
  tar xzf /tmp/${BUILD_PACKAGE} --directory=${DEPLOY_DIR}
  rm /tmp/${BUILD_PACKAGE}

  echo Copy settings.local.php
  mkdir -p ${DEPLOY_DIR}/config
  cp ${DEPLOY_TARGET}/current/config/settings.local.php ${DEPLOY_DIR}/config/settings.local.php

  echo Make files available
  if [[ -d ${DEPLOY_TARGET}/shared/files ]]; then
    ln -s ${DEPLOY_TARGET}/shared/files ${DEPLOY_DIR}/public/sites/default/files
  else
    echo Using the old copy files method
    cp -R ${DEPLOY_TARGET}/current/public/sites/default/files ${DEPLOY_DIR}/public/sites/default/
  fi

#  echo Set correct permissions
#  sudo /usr/local/bin/fix-permissions.sh --drupal_path=${DEPLOY_DIR} --drupal_user=${DRUPAL_USER} --httpd_group=${HTTPD_GROUP}

  ERROR=\$?
  if [[ "\${ERROR}" -ne 0 ]]; then
    exit 1
  fi

  echo Deploying
  ${DRUSH} sset system.maintenance_mode 1

  ERROR=\$?
  if [[ "\${ERROR}" -ne 0 ]]; then
    exit 1
  fi

  echo Backup database to ${BACKUP_DIR}/db.sql.gz
  mkdir -p ${BACKUP_DIR}
  ${DRUSH} sql-dump --gzip > ${BACKUP_DIR}/db.sql.gz

  ERROR=\$?
  if [[ "\${ERROR}" -ne 0 ]]; then
    exit 1
  fi

  if [[ -d ${DEPLOY_TARGET}/shared/files ]]; then
    echo Backup files directory
    cp -R ${DEPLOY_TARGET}/shared/files ${BACKUP_DIR}
  fi

  echo Update database
  ${DRUSH} updatedb -y \
  && ${DRUSH} entity-updates -y \
  && ${DRUSH} config-import -y \
  && ${DRUSH} cache-rebuild

  ERROR=\$?
  if [[ "\${ERROR}" -ne 0 ]]; then
    exit 1
  fi

  echo Change symlink to ${DEPLOY_DIR}
  rm ${DEPLOY_TARGET}/current > /dev/null
  ln -s ${DEPLOY_DIR} ${DEPLOY_TARGET}/current

  ${DRUSH} sset system.maintenance_mode 0
  ${DRUSH} cache-rebuild
  rm ${DEPLOY_DIR}/.DEPLOY

  echo Cleanup previously failed deploys
  find ${DEPLOY_TARGET} -maxdepth 2 -name '.DEPLOY' -printf '%h\n' | xargs rm -rf

  echo Cleanup outdated deploys
  OUTDATED_DEPLOYS=\$(ls -ltr ${DEPLOY_TARGET} | grep "^d.*[0-9]\{8\}-[0-9]\{4\}$" | tr -s ' ' | cut -f9- -d' ' | head -n -3)
  echo \${OUTDATED_DEPLOYS}
  (cd ${DEPLOY_TARGET} && rm -rf \${OUTDATED_DEPLOYS})

  echo Cleanup outdated database and files backups
  OUTDATED_BACKUPS=\$(ls -ltr ${BACKUP_TARGET} | grep "^d.*[0-9]\{8\}-[0-9]\{4\}$" | tr -s ' ' | cut -f9- -d' ' | head -n -3)
  echo \${OUTDATED_BACKUPS}
  (cd ${BACKUP_TARGET} && rm -rf \${OUTDATED_BACKUPS})
EOF
